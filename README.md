# Marsbot Setup files

## Vim Plugins

* Vim Plug
* Vim Mkdir
* Vim Eunuch
* Vim Markdown
* Html5
* Vim CSS3 Syntax
* Vim Closetag
* Closetag.vim
* Emmet vim
* Vim Slim
* Vim Jinja2 Syntax
* SCSS Syntax
* Vim Coloresque
* Vim JSON
* TypeScript Vim
* Tsuquyomi
* Vim Ruby
* Vim Rake
* Vim Rails
* Editorconfig Vim
* Nerdtree
* Vim Nerdtree Syxtax Highlight
* Deoplete
* Deoplete-jedi
* UltiSnips
* Vim Snippets
* Vim NumberToggle
* Vim Airline
* Vim Airline Themes
* Vim Devicons
* Vim GitGutter
* Vim Fugitive
* Nerdtree Git Plugin
* Vim Virtualenv
* Vim Cute Python
* CtrlP
* CtrlP Py Matcher
