#!/bin/sh

install_fonts() {

    if [ "$OS_NAME" = "macOS" ]; then
        execute \
            "unzip -n \"$SCRIPT_PATH/fonts/*.zip\" -d \"$HOME/Library/Fonts\"" \
            "Installing fonts"
    fi
}

remove_windows_fonts() {

    if [ "$OS_NAME" = "macOS" ]; then
        execute "rm $HOME/Library/Fonts/*Windows*" "Removing Windows Compatible Fonts"
    fi
}

main() {

    print_subtitle "Fonts"
    install_fonts
    remove_windows_fonts
}

main
