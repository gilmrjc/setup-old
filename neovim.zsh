#!/bin/zsh

activate_pyenv() {

    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
}

create_python2_virtualenv() {

    # Check if the venv already exists
    if echo "$(pyenv virtualenvs)" | grep -q neovim2; then
        print_success "Virtualenv for Python 2"
    else
        execute "pyenv virtualenv 2.7.13 neovim2" "Virtualenv for Python 2"
    fi
}

create_python3_virtualenv() {

    # Check if the venv already exists
    if echo "$(pyenv virtualenvs)" | grep -q neovim3; then
        print_success "Virtualenv for Python 3"
    else
        execute "pyenv virtualenv 3.6.0 neovim3" "Virtualenv for Python 3"
    fi
}

set_python2_virtualenv() {

    if pyenv activate neovim2 &> /dev/null; then
        execute "pip install neovim" "Installing Neovim wrapper for Python 2"
        pyenv deactivate
    else
        print_error "Could'nt activate Python 2 virutalenv"
    fi
}

set_python3_virtualenv() {

    if pyenv activate neovim3 &> /dev/null; then
        execute "pip install neovim" "Installing Neovim wrapper for Python 3"
        pyenv deactivate
    else
        print_error "Could'nt activate Python 3 virutalenv"
    fi
}

main() {

    print_title "Neovim virtualenvs"

    activate_pyenv
    create_python2_virtualenv
    create_python3_virtualenv
    set_python2_virtualenv
    set_python3_virtualenv
}

main
