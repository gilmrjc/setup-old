#!/usr/bin/env bash

answer_is_yes() {
    [ "$REPLY" = "Y" -o "$REPLY" = "y" ]  && return 0 || return 1
}

ask_for_confirmation() {
    print_question "$1 (y/n)"
    REPLY="$(get_character)"
    printf "\n"
}

ask_sudo() {
    sudo -v

    while true; do
        sleep 60
        sudo -n true
        kill -0 "$$" || exit
    done 2> /dev/null &
}

cmd_exists() {
    command -v "$1" 1>/dev/null 2>&1
}

execute() {
    cmd="$1"
    msg="$2"
    tmp_file="$(mktemp /tmp/XXXX)"
    exit_code=0
    cmd_pid=""

    eval "$cmd" 1>/dev/null 2>"$tmp_file" &
    cmd_pid=$!
    show_spinner "$cmd_pid" "$cmd" "$msg"
    wait "$cmd_pid" 1>/dev/null 2>&1
    exit_code=$?

    print_result $exit_code "$msg"
    if [ -z $exit_code ]; then
        print_success "$msg"
    fi

    rm -rf "$tmp_file"
    return $exit_code
}

get_character() {
    old="$(stty -g)"
    stty raw min 0 time 60
    printf "%s" "$(dd bs=1 count=1 2>/dev/null)"
    stty $old
}

get_current_directory() {
    # Save the current directory
    pushd . > /dev/null

    # Set the path used to call the script
    SCRIPT_PATH="$0";

    # If the script name is a symlink, resolve it
    while [ -L "${SCRIPT_PATH}" ]; do
        cd "$(dirname "${SCRIPT_PATH}")"
        SCRIPT_PATH="$(readlink "$(basename "${SCRIPT_PATH}")")";
    done

    # cd into the script directory
    cd "$(dirname "${SCRIPT_PATH}")" > /dev/null

    # Set the path using the current directory
    SCRIPT_PATH=$(pwd);

    # Restore the working directory
    popd  > /dev/null
}

get_os() {
    # Keep the name of the operative system
    OS_NAME=¨¨

    # Determine the kernel
    KERNEL_NAME="$(uname -s)"

    # Determine the os using the kernel name
    if [ "$KERNEL_NAME" = "Darwin" ]; then
        OS_NAME="macOS"
    else
        OS_NAME="$KERNEL_NAME"
    fi
}

mkd() {
    # Verify that if the file exists
    if [ -n "$1" ]; then
        if [ -e "$1" -o -L "$1" ]; then
            # Verify if it isn't a directory
            if [ ! -d "$1" ]; then
                echo "$1 is a file"
            # If it is, then success
            else
                print_success "$1"
            fi
        # If it doesn't exist, create it
        else
        mkdir -p "$1"
        fi
    fi
}

print_color() {
    printf "%b" "$(tput setaf "$2" 2> /dev/null)" "$1" "$(tput sgr0 2> /dev/null)"
}

print_error() {
    print_in_red "\r   [✖] $1 $2\n"
}

print_in_green() {
    print_color "$1" 2
}

print_in_purple() {
    print_color "$1" 5
}

print_in_red() {
    print_color "$1" 1
}

print_in_yellow() {
    print_color "$1" 3
}

print_question() {
    print_in_yellow "   [?] $1"
}

print_result() {
    if [ "$1" -eq 0 ]; then
        print_success "$2"
    else
        print_error "$2"
    fi
}

print_subtitle() {
    print_in_purple "    - $1\n"
}

print_success() {
    print_in_green "\r   [✔] $1\n"
}

print_title() {
    print_in_purple "\n • $1 \n\n"
}

show_spinner() {
    pid="$1"
    cmd="$2"
    msg="$3"

    spin="/ - \\ |"
    while kill -0 "$pid" 1>/dev/null 2>&1; do
        for c in $spin; do
            printf "\r   [%c] %s" "$c" "$msg"
            sleep 1
        done
    done
}

verify_os() {
    # Get the OS name
    get_os

    if [ "$OS_NAME" = "macOS" ]; then
        return 0
    else
        echo "Sorry, this script does not support your OS"
        return 1
    fi
}
