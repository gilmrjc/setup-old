#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" || exit

main() {
. "../utils.sh"
. "./run_shellcheck.sh"
}

main
