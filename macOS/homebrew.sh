#!/bin/sh

brew_cleanup() {

    execute "brew cleanup" "Homebrew (cleanup)"
    execute "brew cask cleanup" "Homebrew (cask cleanup)"
}

brew_install() {

    # Get the app name and install instruction
    formula_name="$1"
    formula="$2"

    # Check if the app is installed
    if brew list "$formula" 1>/dev/null 2>&1; then
        print_success "$formula_name"
    # If not install if
    else
        execute "brew install $formula" "$formula_name"
    fi
}

brew_update() {

    execute "brew update" "Update Homebrew"
}

brew_upgrade() {

    execute "brew upgrade" "Upgrade system (Homebrew)"
}

cask_install() {

    # Get the app name and install instruction
    formula_name="$1"
    formula="$2"

    # Check if the app is installed
    if brew cask list "$formula" 1>/dev/null 2>&1; then
        print_success "$formula_name"
    # If not install if
    else
        execute "brew cask install $formula" "$formula_name"
    fi

}

install_homebrew() {

    # Check if homebrew is installed
    if cmd_exists "brew"; then
        print_result $? "Install Homebrew"
    # If not install it
    else
        printf "\n" | ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 1>/dev/null 2>&1
    fi
}

main() {

    print_subtitle "Homebrew"

    install_homebrew
    brew_update
    brew_upgrade
}

main
