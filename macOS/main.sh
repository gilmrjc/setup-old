#!/bin/sh

# Install XCode
. "macOS/xcode.sh"

# Install hombrew
. "macOS/homebrew.sh"

# Install apps
. "macOS/apps.sh"
