#!/bin/sh

agree_with_xcode_license() {

    # Accept the xcodebuild license automatically
    sudo xcodebuild -license accept 1>/dev/null 2>&1
    print_result $? "Agree to the terms of the Xcode license"
}

install_xcode() {

    # Check if Xcode is installed
    if xcode_installed; then
        print_success "Xcode.app"
    # If not, open iTunes to install it
    else
        open "macappstores://itunes.apple.com/en/app/xcode/id497799835"
        execute "until xcode_installed; do sleep 5; done" "Xcode.app"
    fi
}

install_xcode_command_line_tools() {

    # If the tools are installed, succeed
    if  xcode_command_line_tools_installed ; then
        print_success "Xcode Command Line Tools"
    # If not, install them
    else
        xcode-select --install 1>/dev/null 2>&1
        execute "until xcode_command_line_tools_installed; do sleep 5; done" \
            "Xcode Command Line Tools"
    fi
}

set_xcode_developer_directory() {

    # Set Xcode active developer directory
    sudo xcode-select -switch "/Applications/Xcode.app/Contents/Developer" \
        1>/dev/null 2>&1
    print_result $? "Configure Xcode active developer directory"
}

xcode_command_line_tools_installed() {

    # Check if the command line tools are installed looking for the
    # path
    xcode-select --print-path 1>/dev/null 2>&1
}

xcode_installed() {

    # Test if Xcode.app is installed
    [ -d "/Applications/Xcode.app" ]
}

main() {

    print_subtitle "Xcode"

    install_xcode_command_line_tools
    install_xcode
    set_xcode_developer_directory
    agree_with_xcode_license
}

main
