#!/bin/sh

install_apps() {

    brew_install "Antigen" "antigen"
    brew_install "Git" "git"
    brew_install "Heroku" "heroku"
    brew_install "Htop" "htop"
    brew_install "Memcached" "memcached"
    brew_install "MongoDB" "mongodb"
    brew_install "Neovim" "neovim/neovim/neovim"
    install_nvm
    brew_install "Postgres" "postgres"
    brew_install "Pyenv" "pyenv"
    brew_install "Pyenv Virtualenv" "pyenv-virtualenv"
    brew_install "Redis" "redis"
    brew_install "Rbenv" "rbenv"
    brew_install "Ruby Build" "ruby-build"
    brew_install "ShellCheck" "shellcheck"
    brew_install "SQLite" "sqlite"
    brew_install "Tree" "tree"
    brew_install "Xz" "xz"
    brew_install "Zsh" "zsh"
    brew_install "Zsh Completions" "zsh-completions"

    cask_install "Google Drive" "google-drive"
    cask_install "iTerm2" "iterm2"
    cask_install "MacDown" "macdown"
    cask_install "VirtualBox" "virtualbox"
}

install_browsers(){

    cask_install "Brave" "brave"
    cask_install "Chrome" "google-chrome"
    cask_install "Chrome Canary" "google-chrome-canary"
    cask_install "Firefox" "firefox"
    cask_install "Firefox Developer Edition" "firefoxdeveloperedition"
    cask_install "Opera" "opera"
    cask_install "Opera Neon" "opera-neon"
    cask_install "Safari Preview" "safari-technology-preview"
}

install_nvm() {

    # Verify if NVM is installed
    if [ -d "$HOME/.nvm" ]; then
        print_success "nvm"
    # If not install it
    else
        execute "curl -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash" "Installing nvm"
    fi
}

main() {

    print_subtitle "Apps"

    install_apps

    print_subtitle "Browsers"

    install_browsers

    print_subtitle "Cleanup"
    brew_cleanup
}

main
