#!/usr/bin/env zsh

mkd() {
    mkdir -p "$1" && cd "$1"
}
