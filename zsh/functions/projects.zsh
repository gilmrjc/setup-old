cdprojects() {
    cd ~/projects
}

cdproject() {

    if [ -d "$HOME/projects/$1" ]; then
        if [ -d "$HOME/projects/$1/master" ]; then
            cd "$HOME/projects/$1/master"
        else
            cd "$HOME/projects/$1"
        fi
    else
        echo "Project not found"
    fi
}

createproject() {

    local namespace="marsbot"
    if [ ! -z $2 ]; then
        namespace=$2
    fi
    if [ -z $1 ]; then
        echo "Provide a project name"
    elif [ -d "$HOME/projects/$namespace/$1" ]; then
        echo "Project already exists"
        return 1
    else
        mkdir -p "$HOME/projects/$namespace/$1/master"
        cdproject "$namespace/$1"
        return 0
    fi
}

cloneproject() {

    local namespace="marsbot"
    if [ ! -z $3 ]; then
        namespace=$3
    fi
    if [ -z $1 ]; then
        echo "Provide a project name"
    elif [ -z $2 ]; then
        echo "Provide a project repo url"
    elif [ -d "$HOME/projects/$namespace/$1" ]; then
        echo "Project already exists"
        return 1
    else
        mkd "$HOME/projects/$namespace/$1"
        git clone "$2" "master"
        cdproject "$namespace/$1"
        return 0
    fi
}

lsprojects() {

    pushd ~/projects &>/dev/null
    for D in */*(/); do
        echo "${D}"
    done
    popd &>/dev/null
}

rmproject() {
    rm -r $1
}

# Git related functions

chbranch() {
    if [ -d "../$1" ]; then
        cd "../$1"
    else
        mkdir "../$1"
    fi
}

## External docker
activate_external_docker() {
  ### Env variables
  export E=/Volumes/Disquillo
  export EHOME=$E/$HOME

  ## Symbolic links

  # Create a symlink to External Drive to preserve internal drive.
  ln -s $EHOME/Library/Containers/com.docker.docker /Users/gil/Library/Containers
}
